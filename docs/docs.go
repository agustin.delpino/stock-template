package docs

import (
	"embed"
	"gitlab.com/fravega-it/logistics/stock/stock-common/swagger/v1"
)

//go:embed docs.yml
var docs embed.FS

// Initialize Swagger documentation
func init() {
	swagger.NewSpecYml(&docs).Init()
}
