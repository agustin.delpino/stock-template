package main

import "gitlab.com/fravega-it/logistics/{project-name}/internal/server"

func main() {
	server.InitServer()
}
