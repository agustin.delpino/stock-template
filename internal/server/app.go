package server

import (
	cmlog "gitlab.com/fravega-it/logistics/stock/stock-common/logger/v1"
	"gitlab.com/fravega-it/logistics/{project-name}/internal/server/infrastructure"
	injectors "gitlab.com/fravega-it/logistics/{project-name}/internal/server/infrastructure/modinjector"
	"go.uber.org/fx"
)

const (
	// startMsg is used to prompt at application start up.
	startMsg = "STARTING {project-name-upper} SERVER"
	// shutdownMsg is used to prompt at application shutdown.
	shutdownMsg = "SHUTDOWN {project-name-upper} SERVER"
)

// InitServer initializes the FX with the Application's HTTP Server.
func InitServer() {
	fx.New(
		NewServer(),
		fx.Invoke(func(a *infrastructure.AppServer, l cmlog.Logger) {
			l.Info(startMsg)
			err := a.Listen()
			if err != nil {
				l.Panic(err.Error())
			}
			l.Info(shutdownMsg)
		}),
	).Run()
}

// NewServer returns the tree dependencies of the whole application.
func NewServer() fx.Option {
	injections := []interface{}{
		infrastructure.NewAppServer,
	}

	injections = append(injections, injectors.GetAppInjector()...)
	injections = append(injections, injectors.GetDomainInjector()...)
	injections = append(injections, injectors.GetInfraInjector()...)

	return fx.Provide(injections...)
}
