package injectors

import (
	"gitlab.com/fravega-it/logistics/{project-name}/internal/server/infrastructure/config"
)

// GetInfraInjector returns the dependencies from infra layer.
func GetInfraInjector() []interface{} {
	return []interface{}{
		config.NewConfig,
		config.NewLogger,
	}
}
