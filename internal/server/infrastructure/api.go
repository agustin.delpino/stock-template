package infrastructure

import (
	"fmt"
	"github.com/gofiber/swagger"
	cmlog "gitlab.com/fravega-it/logistics/stock/stock-common/logger/v1"
	_ "gitlab.com/fravega-it/logistics/{project-name}/docs"
	"gitlab.com/fravega-it/logistics/{project-name}/internal/server/infrastructure/config"
)

const (
	// pong is the response message to the /ping.
	pong = "PONG"
)

// AppServer is the repo server that provides the repo endpoint of the service.
type AppServer struct {
	// logger is used for log.
	logger cmlog.Logger
	// config is the runtime env-config.
	config *config.Config
}

// Listen returns any error some runtime error of the initialized repo server.
// This method initializes the repo server.
func (s *AppServer) Listen() error {
	app := fiber.New()

	app.Get("/ping", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).SendString(pong)
	})

	api := app.Group("/api/v1")
	api.Get("/swagger/*", swagger.HandlerDefault)
	return app.Listen(fmt.Sprintf(":%s", s.config.Port))
}

// NewAppServer returns a new instance of the Application's Server.
func NewAppServer(
	l cmlog.Logger, c *config.Config,
) *AppServer {
	return &AppServer{
		logger: l.Clone().Fields(cmlog.String("logger", "AppServer")),
		config: c,
	}
}
