package config

import (
	cmlog "gitlab.com/fravega-it/logistics/stock/stock-common/logger/v1"
	"os"
)

// NewLogger returns a new instance of Logger with default-global presets.
func NewLogger(c *Config) cmlog.Logger {
	return cmlog.New(&cmlog.Config{
		Level:    cmlog.LevelFrom(c.LogLevel),
		Out:      cmlog.NewSyncOut(os.Stdout),
		MsgKey:   cmlog.DefaultMsgLabel,
		LevelKey: cmlog.DefaultLevelLabel,
	})
}
