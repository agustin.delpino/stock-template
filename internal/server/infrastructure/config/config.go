package config

import (
	"github.com/kelseyhightower/envconfig"
)

// Config contains the env-vars of the service.
type Config struct {
	Environment string `required:"true"`
	LogLevel    string `split_words:"true" default:"DEBUG"`
	LogFormat   string `split_words:"true" default:"JSON"`
	ServiceName string `split_words:"true" required:"true"`
	Port        string `split_words:"true" required:"true"`
}

// NewConfig returns the Config that contains all env-vars.
func NewConfig() *Config {
	var c Config
	if err := envconfig.Process("", &c); err != nil {
		panic(err)
	}
	return &c
}
