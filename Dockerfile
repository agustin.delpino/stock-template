FROM registry.gitlab.com/fravega-it/arquitectura/godocker:1.20.2 AS build

ARG CI_REGISTRY_USER
ARG CI_JOB_TOKEN

WORKDIR /src

COPY go.mod go.sum ./
COPY ./cmd ./cmd
COPY ./internal ./internal
COPY ./docs ./docs

RUN echo "machine gitlab.com login ${CI_REGISTRY_USER} password ${CI_JOB_TOKEN}" > $HOME/.netrc && \
go env -w GOPRIVATE="gitlab.com/fravega-it/*" && \
git config --global url."https://${CI_REGISTRY_USER}:${CI_JOB_TOKEN}@gitlab.com/".insteadOf https://gitlab.com/ && \
go mod download

RUN go mod download

WORKDIR ./cmd

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o /service

# final stage
FROM gcr.io/distroless/static-debian11:latest

COPY --from=build /service /app/
WORKDIR /app

CMD ["./service"]

